#div1 {

height: 150px;

width: 400px;

margin: 20px;

border: 1px solid red;

padding: 10px;

}


1. Total Height:  212px

margin-top 20 + border-top 1 + padding-top 10 + content-height 150 + padding-bottom 10
+ border-bottom 1 + margin-bottom 20 = 212



2. Total Width: 462px

margin-left 20 + border-left 1 + padding-left 10 + content-width 400 + padding-right 10 
+ border-right 1 + margin-right 20 = 462




3. Browser Calculated Width: 422px

border-top 1 + padding-left 10 + content-width 400 + padding-right 10 + border-bottom 1 = 422



4. Browser Calculated Height: 172px

border-top 1 + padding-top 10 + content-height 150 + padding-bottom 10 + border-bottom 1 = 172



